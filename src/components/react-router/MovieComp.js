import React from 'react'
import { useParams } from 'react-router-dom'

export default function MovieComp() {
  const {title} = useParams()  
  return (
    <div><h1>{title}</h1></div>
  )
}
