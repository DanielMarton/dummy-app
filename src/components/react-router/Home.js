import React from 'react'
import HooksExample from './HooksExample'
import PollComponent from './PollComponent'
import SocketComp from './SocketComp'
import SocketWithHooks from './SocketWithHooks'

export default function Home() {
  return (
    <div>
      <h1>Home</h1>
      {/*<PollComponent />*/}
      {/*<SocketComp />*/}
      {/*<HooksExample />*/}
      <SocketWithHooks />
    </div>
  )
}
