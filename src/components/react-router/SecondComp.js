import React from 'react'
import { useParams } from 'react-router-dom'

function SecondComp() {
  const {name} = useParams()
  //Itt az urlbe kell beírni, hogy pl /Joe
  return (
    <div>Hello {name} !</div>
  )
}

export default SecondComp