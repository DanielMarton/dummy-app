import React from 'react'

export default function FirstComp() {
  let longArr = []
  for (let i=0; i<100; i++){
    longArr.push("item no. " + i)
  }
  return (
    <div>
    <h1>FirstComp</h1>
    <div>
      {longArr.map((item) => <div>{item}</div>)}
    </div>
    </div>
  )
}
