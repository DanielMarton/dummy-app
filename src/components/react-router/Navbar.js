import React from 'react'
import { Link } from 'react-router-dom'

export default function Navbar() {
  const name = "Joe"
  const testTitle = "This is a test"
  const testClassComp = "test ClassComp!"
  return (
    <nav >
      <div className='navbar'>
        <Link to="/" className='fancy'>Home</Link>
        <Link to="/first_comp" className='fancy'>FirstComp</Link>
        <Link to={`/${name}`} className='fancy'>Say hello to Joe</Link>
        <Link to={`/testClass/${testClassComp}`} className="fancy">{testClassComp}</Link>
      </div>
    </nav>
  )
}
