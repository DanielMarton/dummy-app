import React from 'react'
import { useState } from 'react'

export default function PollComponent() {

    const [pollAnswers, setPollAnswers] = useState({
        answerA: 0,
        answerB: 0,
        answerC: 0,
        answerD: 0
    })

    const [conesAnswers, setConesAnswers] = useState({
        basic: 0,
        sweet: 0
    })

    //az egyik függvényben nem férhetek hozzá a másik constanshoz, kvázi a másik state-hez
    //a setPollAnswers previousState = a pollAnswers
    //a setConesAnswers nél pedig conesAnswers

    const [isLiked, setIsLiked] = useState(false)   //itt ez a kezdőérték

    const updateAnswerA = () => {
        setPollAnswers(previousState => {
            return { ...previousState, answerA: previousState.answerA + 1 }
        })
    }

    const updateAnswerB = () => {
        setPollAnswers(previousState => {
            return { ...previousState, answerB: previousState.answerB + 1 }
        })
    }

    const updateBasic = () => {
        setConesAnswers(previousState => {
            return { ...previousState, basic: previousState.basic + 1 }
        })
    }

    return (
        <div>
            <h1>What's your favourite ice cream?</h1>
            <p>Chocolate: {pollAnswers.answerA}</p>
            <p>Vanilla: {pollAnswers.answerB}</p>
            <button onClick={updateAnswerA}>Chocolate</button>
            <button onClick={updateAnswerB}>Vanilla</button>

            <h1>What's your favourite cone?</h1>
            <p>basic: {conesAnswers.basic}</p>
            <p>sweet: {conesAnswers.sweet}</p>
            <button onClick={updateBasic}>basic</button>


            <button onClick={() => setIsLiked(!isLiked)}>like</button>
            <p> {isLiked ? "liked" : "not liked"}</p>
        </div>
    )
}
