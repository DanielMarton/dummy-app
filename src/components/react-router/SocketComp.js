import React, { Component } from 'react'


//const socket = new WebSocket('wss://ws.finnhub.io?token=c1mrjdi37fktai5sgaog')

 class SocketComp extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         lastPrice : 0,
         downloads: 0
      }

      this.updatePrice = this.updatePrice.bind(this)
      this.unsubscribe = this.unsubscribe.bind(this)
      
    }

    componentDidMount(){
        this.socket = new WebSocket('wss://ws.finnhub.io?token=c1mrjdi37fktai5sgaog')

        this.socket.addEventListener("open", (event)=>{
            //this.socket.send(JSON.stringify({'type':'subscribe', 'symbol': 'AAPL'}))
            this.socket.send(JSON.stringify({'type':'subscribe', 'symbol': 'BINANCE:BTCUSDT'}))
            //this.socket.send(JSON.stringify({'type':'subscribe', 'symbol': 'IC MARKETS:1'}))
        })

        this.socket.addEventListener("message", (event)=>{
            const tempData = JSON.parse(event.data)
            console.log(tempData.data[0].p)
            this.updatePrice(tempData.data[0].p)
            
        })
    }

    unsubscribe(){
      console.log("unsubscribe")
      this.socket.send(JSON.parse({'type':'unsubscribe','symbol': 'BINANCE:BTCUSDT'}))
    }
    componentWillUnmount(){
       //this.socket.send(JSON.stringify({'type':'unsubscribe','symbol': 'AAPL'}))
       
       this.socket.send(JSON.parse({'type':'unsubscribe','symbol': 'BINANCE:BTCUSDT'}))
    }

    updatePrice(price) {

        console.log(typeof price)
        let copiedTempState = { ...this.state }
        copiedTempState.lastPrice = price
        this.setState(copiedTempState)
      }

  render() {
    return (
      <div><h1>SocketComp</h1>
        <h2>Last price: {this.state.lastPrice}</h2>
      </div>
      
    )
  }
}

export default SocketComp