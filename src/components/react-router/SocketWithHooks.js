import React from 'react'
import { useState, useEffect, useRef } from "react"

function SocketWithHooks() {

    const [stockData, setStockData] = useState({})
    const socket = useRef()

    useEffect(() => {
        console.log("componentDidMount")

        socket.current = new WebSocket('wss://ws.finnhub.io?token=c1mrjdi37fktai5sgaog')
        socket.current.addEventListener("open", (event) => {
            socket.current.send(JSON.stringify({ 'type': 'subscribe', 'symbol': 'BINANCE:BTCUSDT' }))
        })
        socket.current.addEventListener("message", (event) => {
            try {
                const tempData = JSON.parse(event.data)
                if (tempData) { console.log(tempData.data[0]) }
                setStockData(tempData.data[0])
            } catch(error){
                console.log(error)
            }         
        })

    }, [])

    useEffect(() => {
        console.log("componentDidUpdate")
    })

    useEffect(() => {
        return () => {
            console.log("componentWillUnmount")
            socket.current.send(JSON.stringify({ 'type': 'unsubscribe', 'symbol': 'BINANCE:BTCUSDT' }))
            socket.current.close()
        }
    }, [])

    return (
        <div><h1>{stockData.p}</h1></div>
    )
}

export default React.memo(SocketWithHooks) 