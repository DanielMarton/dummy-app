import React from 'react'
import { Link } from 'react-router-dom'

function Titles() {
    const movieTitles = [
        "Jurassic_Park", "Mad_Max", "Monster_Hunter"
    ]
    return (
        <div className='titles'>
            {movieTitles.map(
                movieTitle => <Link to={`/films/${movieTitle}`} className="link">{movieTitle}</Link>
            )}

        </div>
    )
}

export default Titles