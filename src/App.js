import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Navbar from './components/react-router/Navbar';
import Home from './components/react-router/Home';
import FirstComp from './components/react-router/FirstComp';
import SecondComp from './components/react-router/SecondComp';
import MovieComp from './components/react-router/MovieComp';
import Titles from './components/Titles';
import ClassComp from './components/react-router/ClassComp';

function App() {
  return (
    <Router>
      <div className='App'>
        <Navbar />
        <Titles />
        <div>
          <h1>Some title</h1>
          <div className='routesDiv'>
            <Routes>{/*tehát a routes a megjelenítő*/}
              <Route exact path="/" element={<Home />} />
              <Route exact path='/first_comp' element={<FirstComp />} />
              <Route exact path="/:name" element={<SecondComp />} />
              <Route exact path="/films/:title" element={<MovieComp />} />
              <Route exact path="/test/:title" element={<MovieComp />} />
              <Route exact path="/testClass/:testClassComp" element={<ClassComp />} />
            </Routes>
          </div>

        </div>
      </div>
    </Router>
  );
}

export default App;
